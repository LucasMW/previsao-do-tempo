import 'package:flutter/material.dart';
import 'package:previsao_do_tempo/API/model/forecast.dart';
import 'package:previsao_do_tempo/API/model/weather.dart';
import 'package:previsao_do_tempo/components/weather_widget.dart';
import 'package:previsao_do_tempo/constants.dart';

import 'API/api.dart';
import 'location_service.dart';

const String celsius = "°C";

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  WeatherResponse? _weather;
  Forecast? _forecast;
  double _myLatitude = Constants.defaultLat;
  double _myLongitude = Constants.defaultLong;

  @override
  void initState() {
    super.initState();

    //Consiga a localização do usuário e após isso, a previsão do tempo.
    getLocation().then((_) {
      updateForecast(lat: _myLatitude, long: _myLongitude);
    });
  }

  Future<void> getLocation() async {
    final locationService = LocationService();
    try {
      await locationService.handlePermissions();
      final location = await locationService.getUserLocation();
      if (location?.latitude != null && location?.longitude != null) {
        setState(() {
          _myLatitude = location!.latitude!;
          _myLongitude = location.longitude!;
        });
      }
    } catch (problem) {
      print(problem);
      _myLatitude = Constants.defaultLat;
      _myLongitude = Constants.defaultLong;
    }
  }

  void updateForecast({double? lat, double? long}) {
    API().getForecast(lat: lat, long: long).then((forecast) {
      setState(() {
        _forecast = forecast;
        _weather = forecast.days.first;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final String? fTemperatura =
        _weather != null ? _weather!.temperature.toStringAsFixed(1) : null;
    final String? fMaxima =
        _weather != null ? _weather!.maxima.toStringAsFixed(1) : null;
    final String? fMinima =
        _weather != null ? _weather!.minima.toStringAsFixed(1) : null;
    final String? fSensacao =
        _weather != null ? _weather!.sensacao.toStringAsFixed(1) : null;

    //Preencheendo os próximos dias.
    List<Widget> widgets = [];
    for (final day in _forecast?.days ?? []) {
      widgets.add(Padding(
        padding: const EdgeInsets.all(8.0),
        child: WeatherWidget(iconString: day.iconString),
      ));
    }

    /* Esse código comentado é equivalente ao loop anterior: */

    // widgets = _forecast?.days
    //         .map((e) => WeatherWidget(iconString: e.iconString))
    //         .toList() ??
    //     [];

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: _weather != null && _forecast != null
            ? Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.black12,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Column(
                      children: [
                        Text("Previsão Diária:"),
                        SizedBox(
                          width: MediaQuery.of(context).size.width,
                          height: 50,
                          child: Center(
                            child: ListView(
                              scrollDirection: Axis.horizontal,
                              children: widgets,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      margin: EdgeInsets.fromLTRB(20, 20, 20, 20),
                      //color: Colors.black12,
                      decoration: BoxDecoration(
                          color: Colors.black12,
                          borderRadius: BorderRadius.circular(20)),
                      child: Center(
                        child: Text(
                          '$fTemperatura $celsius',
                          style: TextStyle(
                              fontSize: 40, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Expanded(
                          flex: 5,
                          child: Container(
                            color: Colors.lightBlue.shade100,
                            child: Center(
                              child: Text(
                                'MIN $fMinima $celsius',
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.w300),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Container(
                            width: 10,
                            height: 20,
                            //color: Colors.black,
                          ),
                        ),
                        Expanded(
                          flex: 5,
                          child: Container(
                            color: Colors.lightGreen.shade100,
                            child: Center(
                              child: Text(
                                'MAX $fMaxima $celsius',
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.w400),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              color: Colors.black12,
                              borderRadius: BorderRadius.circular(2)),
                          child: Text(
                            'A sensação térmica é ',
                          ),
                        ),
                        SizedBox(width: 10),
                        Container(
                          decoration: BoxDecoration(
                              color: Colors.black12,
                              borderRadius: BorderRadius.circular(20)),
                          child: Text(
                            '$fSensacao $celsius',
                            style: TextStyle(
                                fontSize: 30, fontWeight: FontWeight.w500),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )
            : const CircularProgressIndicator(),
      ),
    );
  }
}
