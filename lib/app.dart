import 'package:flutter/material.dart';
import 'package:previsao_do_tempo/splash_screen.dart';

import 'constants.dart';
import 'home_page.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Previsão do tempo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: Constants.splashPage,
      routes: {
        Constants.homePage: (BuildContext context) =>
            const MyHomePage(title: 'Previsão do Tempo'),
        Constants.splashPage: (BuildContext context) => const SplashScreen()
      },
    );
  }
}
