import 'package:location/location.dart';

class LocationService {
  late bool _serviceEnabled = false;
  late PermissionStatus? _permissionGranted;
  late LocationData? _locationData;

  double? latitude;
  double? longitude;

  Future<void> handlePermissions() async {
    Location location = Location();
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }
  }

  Future<LocationData?> getUserLocation() async {
    Location location = Location();
    _locationData = await location.getLocation();
    latitude = _locationData?.latitude;
    longitude = _locationData?.longitude;
    return _locationData;
  }
}
