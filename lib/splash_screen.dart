import 'dart:async';

import 'package:flutter/material.dart';
import 'package:previsao_do_tempo/constants.dart';
import 'package:previsao_do_tempo/home_page.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(const Duration(seconds: 1), () {
      // Navigator.pushReplacement(
      //   context,
      //   MaterialPageRoute<void>(
      //     builder: (BuildContext context) =>
      //         const MyHomePage(title: 'Previsão do Tempo'),
      //   ),
      // );
      Navigator.pushNamed(context, Constants.homePage);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Image.asset(Constants.splashImage),
      ),
    );
  }
}
