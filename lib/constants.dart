class Constants {
  static const String key = "28db145d735c81ba39990f9c07eca878";
  static const defaultLat = -12.9696332;
  static const defaultLong = -38.512657;

  //Assets
  static const String splashImage = "assets/splash.png";

  //Telas
  static const String homePage = "home";
  static const String splashPage = "splash";
}
