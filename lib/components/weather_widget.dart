import 'package:flutter/material.dart';

class WeatherWidget extends StatelessWidget {
  final String iconString;
  const WeatherWidget({Key? key, required this.iconString}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final String iconUrl =
        "http://openweathermap.org/img/wn/$iconString@2x.png";
    return Container(
      decoration: BoxDecoration(
          color: Colors.black12, borderRadius: BorderRadius.circular(20)),
      child: Image.network(
        iconUrl,
        scale: 1,
      ),
    );
  }
}
