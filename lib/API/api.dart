import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:previsao_do_tempo/API/model/forecast.dart';
import 'package:previsao_do_tempo/API/model/weather.dart';

import 'package:previsao_do_tempo/constants.dart';

class API {
  static const key = Constants.key;
  static const defaultLat = Constants.defaultLat;
  static const defaultLong = Constants.defaultLong;

  //https://api.openweathermap.org/data/2.5/onecall?lat={lat}&lon={lon}&appid={API key}

  //Pega a previsão do tempo da semana + hoje pelas coordenadas
  Future<Forecast> getForecast(
      {double? lat = defaultLat, double? long = defaultLong}) async {
    final urlString =
        "https://api.openweathermap.org/data/2.5/onecall?lat=$lat&lon=$long&appid=$key";
    print(urlString);
    final url = Uri.parse(urlString);
    final resposta = await http.get(url);
    final object = jsonDecode(resposta.body);
    final forecast = Forecast.fromObject(object);
    print(forecast.days.length);
    return forecast;
  }

  //pega o tempo atual pelo nome da cidade e país
  Future<WeatherResponse?> getWeatherAtCity(
      String city, String countryCode) async {
    final urlString =
        "https://api.openweathermap.org/data/2.5/weather?q=$city,$countryCode&appid=$key";
    print(urlString);
    final url = Uri.parse(urlString);
    final resposta = await http.get(url);
    final object = jsonDecode(resposta.body);
    final weather = WeatherResponse.fromObject(object);
    return weather;
  }

  //pega o tempo atual pelas coordenadas
  Future<WeatherResponse?> getWeather(
      {double? lat = defaultLat, double? long = defaultLong}) async {
    final urlString =
        "https://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$long&appid=$key";
    final url = Uri.parse(urlString);
    final resposta = await http.get(url);
    final object = jsonDecode(resposta.body);
    final weather = WeatherResponse.fromObject(object);
    return weather;
  }
}
