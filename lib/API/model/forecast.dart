import 'package:previsao_do_tempo/API/model/weather.dart';

//Previsão do tempo
class Forecast {
  late List<WeatherResponse> days;

  Forecast(this.days);

  Forecast.fromObject(Map<String, dynamic> object) {
    final array =
        object["daily"] as List<dynamic>; //considerar isso como uma lista
    final List<WeatherResponse> list = [];
    for (final element in array) {
      final temp = element["temp"]["day"] - 272.15;
      final min = element["temp"]["min"] - 272.15;
      final max = element["temp"]["max"] - 272.15;
      final icon = element["weather"][0]["icon"];
      final feels = element["feels_like"]["day"] - 272.15;
      final weather = WeatherResponse(temp, min, max, feels, icon);
      list.add(weather);
    }
    days = list;
  }
}
