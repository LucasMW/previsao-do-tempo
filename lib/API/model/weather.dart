class WeatherResponse {
  late final double temperature;
  late final double minima;
  late final double maxima;
  late final double sensacao;
  late final String iconString;

  WeatherResponse(this.temperature, this.minima, this.maxima, this.sensacao,
      this.iconString);

  WeatherResponse.fromObject(Map<String, dynamic> object) {
    temperature = object['main']['temp'] - 272.15;
    minima = object['main']['temp_min'] - 272.15;
    maxima = object['main']['temp_max'] - 272.15;
    sensacao = object['main']['feels_like'] - 272.15;
    iconString = object['weather'][0]['icon'];
  }
}
